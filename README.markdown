Topspin Drupal Module
===

A module that integrates with Topspin's (http://topspinmedia.com) REST API.
 
Topspin API Documentation: http://docs.topspin.net


PROJECT PAGE
===

http://drupal.org/project/topspin
 
 
REQUIREMENTS
===

* PHP 5.2.x
* jSON enabled for PHP
* cURL enabled for PHP
* A Topspin account with an API key.


FEATURES
===

* Connection API for making requests from the REST API.
  topspin_request($method, $parameters);

* A general configuration form at admin/settings/topspin.
* Topspin content import at admin/content/topspin.
* Default store at admin/settings/topspin/store/1 and ability to create additional stores.
* Most Popular as well as individual offers can be configured as blocks from admin/settings/topspin and then enabled in a region at admin/build/block.

USAGE
===

The following steps will result in a functioning store front showing imported Topspin content.

* Enable the Topspin Media and Topspin Media CCK modules at admin/build/modules.
* Now set up your Topspin credentials and choose which artists you want to pull content from at admin/settings/topspin.
* If you selected "Check for new offers and update automatically", you can wait until a cron run imports your content. Regardless of which option you chose, you can always manually import content at admin/content/topspin.
* Now you can configure your store front. You can configure the default store admin/settings/topspin/store/1 or you can create a new one admin/settings/topspin/store/add.
* You are finished. Go to your store path to see your new store front.

AUTHOR/MAINTAINER
===

* 80 Elements Entertainment (http://80elements.com)
* Topspin Media (http://topspinmedia.com)

