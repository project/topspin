Drupal.behaviors.topspin = function() {
  
  var blocks_selector = '#edit-topspin-store-custom-blocks';
  var blocks_cont_selector = '.topspin-enabled-offer-blocks-wrapper';
  var popular_selector = '#edit-topspin-store-create-most-popular-block';
  var popular_cont_selector = '.topspin-store-popular-blocks-wrapper';
  
  function topspin_toggle(selector, transition) {
    $(selector).toggle(transition);
  }
  
  $(blocks_selector).click(function(e) {
    topspin_toggle(blocks_cont_selector, 'fast');
  });
  $(popular_selector).click(function(e) {
    topspin_toggle(popular_cont_selector, 'fast');
  });
  
  if ($(blocks_selector).length) {
    if (!$(blocks_selector)[0].checked) {
      topspin_toggle(blocks_cont_selector);
    }
  }
  if ($(popular_selector).length) {
    if (!$(popular_selector)[0].checked) {
      topspin_toggle(popular_cont_selector);
    }
  }
  
}

Drupal.behaviors.topspinCart = function(context) {
  if (Drupal.settings.topspin.showEmptyCart && !("admin" in Drupal) && typeof(TSCart) != "undefined") {
    TSCart.showWhenEmpty = true;
  }
}

TSdrupal = {};
TSdrupal.hideFlash = function() {
  // Make flash objs invisible.
  $('object').each(function(i) {
    $($(this)).css('visibility', 'hidden');
  });
}
TSdrupal.showFlash = function() {
  // Make flash objs visible.
  $('object').each(function(i) {
    $($(this)).css('visibility', 'visible');
  });
}
