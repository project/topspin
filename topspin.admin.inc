<?php
// $Id: $

/**
 *
 *  @file
 *  Administrative functionality for Topspin Media module.
 *
 */

define(TOPSPIN_STORE_CONFIG_CREATE_BUTTON, 'Save and Create Store');
define(TOPSPIN_STORE_CONFIG_NORMAL_BUTTON, 'Save');

/**
 * Helper function to store individual forms for multistep wizard.
 *
 * @param array $form_state
 * @return array
 */
function topspin_store_wizard_form_storage($id) {
  $forms = array();
  $view_values = topspin_store_wizard_view_configs();
  // Get all offers.
  $all_result = topspin_request('offers');
  $all_artists = array();
  
  if ($all_result->offers) {
    foreach ($all_result->offers as $index => $offer) {
      if (!isset($all_artists[$offer->artist_id])) {
        $all_artists[$offer->artist_id] = array();
      }
      $all_artists[$offer->artist_id][] = $offer;
    }
  }
  
  // Get artist specific offers.
  $params = array();
  $artist_id = variable_get('topspin_api_artist_id', NULL);
  if (!empty($artist_id)) {
    $params['artist_id'] = $artist_id;
  }
  
  $result = topspin_request('offers', $params);
  
  $artists = array();
  if ($result->offers) {
    foreach ($result->offers as $index => $offer) {
      if (!isset($artists[$offer->artist_id])) {
        $artists[$offer->artist_id] = array();
      }
      $artists[$offer->artist_id][] = $offer;
    }
  }
  
  // Form 1
  $forms['general']['topspin_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#description' => t('Please enter your Topspin store API key.'),
    '#required' => TRUE,
    '#default_value' => variable_get('topspin_api_key', ''),
  );
  $forms['general']['topspin_api_user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#description' => t('Please enter your Topspin store username.'),
    '#required' => TRUE,
    '#default_value' => variable_get('topspin_api_user', ''),
  );
  $artist_options = array('' => t('All artists'));
  foreach (array_keys($all_artists) as $artist_id) {
    $artist_options[$artist_id] = $all_artists[$artist_id][0]->artist_name;
  }
  $forms['general']['topspin_api_artist_id'] = array(
    '#type' => 'radios',
    '#title' => t('Choose the artist to import offers from'),
    '#description' => t("If your Topspin account is associated with more than one artist, you can choose to import all offers from all artists, or select a specific artist to import offers from."),
    '#options' => $artist_options,
    '#required' => FALSE,
    '#default_value' => variable_get('topspin_api_artist_id', ''),
  );
  $forms['general']['topspin_cck_offer_import_publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check for new offers and update all store content automatically?'),
    '#description' => t('If selected, Drupal will periodically pull all offers from Topspin and refresh the contents of your store automatically, overwriting any existing content. To force Drupal to do this now, go !content.', array('!content' => l('here', 'admin/content/topspin'))),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => variable_get('topspin_cck_offer_import_publish', 0),
  );
  $forms['general']['topspin_show_empty_cart'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the Topspin shopping cart at all times, even when empty?'),
    '#description' => t("Should the cart UI appear when it's empty?"),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => variable_get('topspin_show_empty_cart', 0),
  );
  $forms['general']['popular_block'] = array(
    '#type' => 'fieldset',
    '#title' => t("'Most Popular' products block"),
    '#description' => t('Choose how many offers should appear in your Most Popular products block. In order for the block to appear, you must place the "topspin_store: Popular block" at <a href="/admin/build/block">/admin/build/block</a>.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $forms['general']['popular_block']['topspin_store_popular_block_offers_count'] = array(
    '#type' => 'select',
    '#title' => t('How many products in that block?'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    '#description' => t(''),
    '#prefix' => '<div class="topspin-store-popular-blocks-wrapper">',
    '#suffix' => '</div>',
    '#default_value' => $view_values['topspin_store_popular_block_offers_count'],
  );
  
  // Blocks.
  $forms['general']['block'] = array(
    '#type' => 'fieldset',
    '#title' => t("Extra blocks"),
    //'#description' => t(''),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $forms['general']['block']['topspin_store_custom_blocks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add more custom product blocks to your site?'),
    '#description' => t('Choose which offers you would like to be usable as a block. Once enabled, you must place the blocks at <a href="/admin/build/block">/admin/build/block</a>. You can also add/edit block titles.'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => variable_get('topspin_store_custom_blocks', 0),
  );
  
  // Create an array by artists.
  // Show offers organized by artist.
  $artists_indexed = array_keys($artists);
  foreach ($artists as $artist_id => $offers) {
    foreach ($offers as $index => $offer) {
      if ($offer->offer_type != 'buy_button') {
        continue;
      }
      // Add artist fieldset if needed. We do it here so that we have easy access to the artist name.
      if (!isset($forms['general']['block'][$artist_id])) {
        $prefix = '';
        $suffix = '';
        if ($artist_id == $artists_indexed[0]) {
          $prefix = '<div class="topspin-enabled-offer-blocks-wrapper">';
        }
        if ($artist_id == $artists_indexed[count($artists_indexed)-1]) {
          $suffix = '</div>';
        }
        $forms['general']['block'][$artist_id] = array('#type' => 'fieldset', '#title' => check_plain($offer->artist_name), '#collapsible' => TRUE, '#collapsed' => FALSE, '#prefix' => $prefix, '#suffix' => $suffix);
      }
      // Add all "block enable" checkboxes.
      $forms['general']['block'][$artist_id]['topspin_enabled_offer_block_'. $offer->id] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($offer->name),
        '#default_value' => variable_get('topspin_enabled_offer_block_'. $offer->id, 0),
      );
    }
  }
  $forms['general']['topspin_store_twitter_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter username'),
    '#description' => t("Insert your Twitter username if you'd like it to be included in the message sent by the 'Tweet This' button on an offer details page."),
    '#default_value' => variable_get('topspin_store_twitter_user', ''),
  );
  $forms['general']['topspin_store_twitter_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Twitter message'),
    '#description' => t("Customize the 'Tweet This' message.<br />Available tokens: <strong>[title]</strong>, <strong>[site-name]</strong>, <strong>[domain-name]</strong>"),
    '#default_value' => variable_get('topspin_store_twitter_message', 'I just read [title]'),
  );
  
  // Form 2
  $display_id = 'page_'. arg(4);
  // Now that we have total form count, add fieldset titles.
  $forms['store']['topspin_store_url'] = array(
    '#type' => 'textfield',
    '#title' => t('What path do you want the store on?'),
    '#description' => t("Do not enter the full URL. If you want your store to live at <strong>". $GLOBALS['base_url'] ."/store</strong> then enter just <strong>store</strong>."),
    '#required' => TRUE,
    '#default_value' => $view_values['view'][$display_id]['topspin_store_url'],
  );
  $forms['store']['topspin_store_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('What is the page title of the store?'),
    '#required' => FALSE,
    '#default_value' => $view_values['view'][$display_id]['topspin_store_page_title'],
  );
  $forms['store']['topspin_store_column_count'] = array(
    '#type' => 'select',
    '#title' => t('How many offers per row?'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    '#description' => t("How many columns on a page?"),
    '#default_value' => $view_values['view'][$display_id] ? $view_values['view'][$display_id]['topspin_store_column_count'] : 2,
    '#required' => TRUE,
  );
  $forms['store']['topspin_store_row_count'] = array(
    '#type' => 'select',
    '#title' => t('How many rows per page?'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)),
    '#description' => t('This sets the number of offer rows per page. Columns are fixed at two.'),
    '#default_value' => $view_values['view'][$display_id] ? $view_values['view'][$display_id]['topspin_store_row_count'] : 3,
    '#required' => TRUE,
  );
  $offer_type_options = array(
    'buy_button' => 'Purchase Button',
    'email_for_media' => 'Email for Media',
    'fb_for_media' => 'Facebook for Media',
    'bundle_widget' => 'Streaming Player',
    'single_track_player_widget' => 'Single Track Player',
  );
  $forms['store']['topspin_api_show_offer_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose which offer types to show'),
    '#prefix' => '<div class="topspin-api-offer-types-selection-wrapper">',
    '#suffix' => '</div>',
    '#description' => t(""),
    '#options' => $offer_type_options,
    '#default_value' => variable_get('topspin_api_show_offer_types-'. $display_id, array('buy_button')),
  );
  $tags_options = array();
  $tags_vid = topspin_taxonomy_get_vid('tags');
  if ($all_result->offers) {
    foreach ($all_result->offers as $index => $offer) {
      if ($offer->tags) {
        foreach ($offer->tags as $tag) {
          $terms = taxonomy_get_term_by_name($tag);
          foreach ($terms as $term) {
            if ($term->vid == $tags_vid) {
              $tags_options[$term->tid] = $tag;
              break;
            }
          }
        }
      }
    }
  }
  $forms['store']['topspin_store_display_categories_group_tags'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select tags to build categories'),
    '#description' => t("If none are selected, all tags will be used.<br />Need to update this list of tags? Please go !ts-content and re-import all Topspin content.", array('!ts-content' => l('here', 'admin/content/topspin'))),
    '#options' => $tags_options,
    '#default_value' => $view_values['view'][$display_id] ? $view_values['view'][$display_id]['topspin_store_display_categories_group_tags'] : array(),
  );
  $forms['store']['topspin_store_display_categories_group_order'] = array(
    '#type' => 'radios',
    '#title' => t('Display products in what order within categories?'),
    '#description' => t(""),
    '#options' => array(
      'newest_to_oldest' => t('Newest to Oldest'),
      'oldest_to_newest' => t('Oldest to Newest'),
      'custom' => t('Custom (weights must be set in Product nodes - !link)', array('!link' => l('admin/content/node', 'admin/content/node'))),
    ),
    '#default_value' => $view_values['view'][$display_id] ? $view_values['view'][$display_id]['topspin_store_display_categories_group_order'] : 'newest_to_oldest',
  );
  $forms['store']['topspin_store_details_page_per_offer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create a details page for all offers?'),
    '#description' => t(""),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#default_value' => $view_values['view'][$display_id] ? $view_values['view'][$display_id]['topspin_store_details_page_per_offer'] : 1,
  );
  
  return $forms[$id];
}


/**
 * System settings form.
 * 
 * @return struct
 * - Settings form array.
 */
function topspin_store_wizard_form(&$form_state, $id) {
  
  $button_value = TOPSPIN_STORE_CONFIG_NORMAL_BUTTON;
  
  switch ($id) {
    case 'general':
      $form = topspin_store_wizard_form_storage('general');
      break;
    default:
      $form = topspin_store_wizard_form_storage('store');
      break;
  }
  
  if (arg(4) == 'add') {
    $store_view = views_get_view('topspin_store', TRUE);
    $ids = array();
    foreach (array_keys($store_view->display) as $index => $id) {
      if (strpos($id, 'page_') === 0) {
        $ids[] = array_pop(explode('_', $id));
      }
    }
    rsort($ids);
    $next_id = $ids[0]+1;
    $cur_display_id = 'page_'. $next_id;
    $form['#redirect'] = 'admin/settings/topspin/store/'. $next_id;
  }
  
  drupal_set_title(t('Topspin store configuration'));
  
  drupal_add_css(drupal_get_path('module', 'topspin') .'/css/topspin.css', 'theme');
  drupal_add_js(drupal_get_path('module', 'topspin') .'/js/general.js');
  
  $form['buttons']['done'] = array(
    '#type' => 'submit',
    '#value' => $button_value,
  );
  
  return $form;
}

function topspin_store_wizard_form_submit(&$form, &$form_state) {
  // Save form values.
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'topspin_') === 0) {
      variable_set($key, $value);
    }
  }
  
  // Apply configurations to Store view and whatever else needs to be modified.
  topspin_store_wizard_view_configs($form_state['values']);
  
  drupal_set_message(t('Configuration saved.'));
}

function topspin_store_wizard_view_configs($values = NULL) {
  $cur_display_id = 'page_'. arg(4);
  // Get view and set .
  $store_view = views_get_view('topspin_store', TRUE);
  
  $existing_values = array();
  
  // Get category type settings.
  $tags_vid = topspin_taxonomy_get_vid('tags');
  
  foreach ($store_view->display as $display_id => $display) {
    if (strpos($display_id, 'page_') === 0) {
      $existing_values['view'][$display_id]['topspin_store_url'] = $store_view->display[$display_id]->display_options['path'];
      $existing_values['view'][$display_id]['topspin_store_page_title'] = $store_view->display[$display_id]->display_options['title'];
      $existing_values['view'][$display_id]['topspin_store_column_count'] = $store_view->display[$display_id]->display_options['style_options']['columns'];
      if ($existing_values['view'][$display_id]['topspin_store_column_count']) {
        $existing_values['view'][$display_id]['topspin_store_row_count'] = floor($store_view->display[$display_id]->display_options['items_per_page'] / $existing_values['view'][$display_id]['topspin_store_column_count']);
      }
      $existing_values['view'][$display_id]['topspin_store_display_categories_group'] = 'tags';
      $fixed_args = explode('+', $store_view->display[$display_id]->display_options['arguments']['tid']['default_argument_fixed']);
      $existing_values['view'][$display_id]['topspin_store_display_categories_group_tags'] = $fixed_args;
      
      // Get category sort settings.
      $sorts = $store_view->display[$display_id]->display_options['sorts'];
      if ($sorts) {
        $sorts_keys = array_keys($store_view->display[$display_id]->display_options['sorts']);
        if ($sorts_keys[0] == 'created') {
          if ($sorts['created']['order'] == 'DESC') {
            $sort_order = 'newest_to_oldest';
          }
          else {
            $sort_order = 'oldest_to_newest';
          }
        }
        else if ($sorts_keys[0] == 'name') {
          $sort_order = 'spin_tag';
        }
        else if ($sorts_keys[0] == 'field_topspin_weight_value') {
          $sort_order = 'custom';
        }
        $existing_values['view'][$display_id]['topspin_store_display_categories_group_order'] = $sort_order;
      }
      
      $existing_values['view'][$display_id]['topspin_store_details_page_per_offer'] = $store_view->display[$display_id]->display_options['fields']['title']['alter']['make_link'];
    }
  }
  
  $existing_values['topspin_store_popular_block_offers_count'] = $store_view->display['block_1']->display_options['items_per_page'];
  
  // We're setting view configs.
  if ($values) {
    $tax_menu_rebuild_needed = 0;
    
    // Copy a page display if doesn't exist.
    if ($cur_display_id == 'page_add') {
      $ids = array();
      foreach (array_keys($store_view->display) as $index => $id) {
        if (strpos($id, 'page_') === 0) {
          $ids[] = array_pop(explode('_', $id));
        }
      }
      rsort($ids);
      $next_id = $ids[0]+1;
      $cur_display_id = 'page_'. $next_id;
      
      $store_view->display[$cur_display_id] = clone $store_view->display['page_'. $ids[0]];
      
      $store_view->display[$cur_display_id]->id = $cur_display_id;
    }

    if (strpos($cur_display_id, 'page_') === 0) {
      // Save offer types filter.
      if (isset($values['topspin_api_show_offer_types'])) {
        variable_set('topspin_api_show_offer_types-'. $cur_display_id, $values['topspin_api_show_offer_types']);
      }
      // Make modifications.
      if (isset($values['topspin_store_url'])) {
        if ($values['topspin_store_url'] != $existing_values['view'][$cur_display_id]['topspin_store_url']) {
          $tax_menu_rebuild_needed = 1;
        }
        $store_view->display[$cur_display_id]->display_options['path'] = $values['topspin_store_url'];
      }
      if (isset($values['topspin_store_page_title'])) {
        $store_view->display[$cur_display_id]->display_options['title'] = $values['topspin_store_page_title'];
      }
      if (isset($values['topspin_store_column_count'])) {
        $store_view->display[$cur_display_id]->display_options['style_options']['columns'] = $values['topspin_store_column_count'];
      }
      if (isset($values['topspin_store_row_count'])) {
        $columns = $values['topspin_store_column_count'];
        if (!$columns) {
          $columns = $existing_values['view'][$cur_display_id]['topspin_store_column_count'];
        }
        $store_view->display[$display_id]->display_options['items_per_page'] = $values['topspin_store_row_count'] * $columns;
      }
      
      if (isset($values['topspin_store_display_categories_group_tags'])) {
        $tids = array_filter($values['topspin_store_display_categories_group_tags']);
        if (empty($tids)) {
          $tids = array('all');
        }
        $store_view->display[$cur_display_id]->display_options['arguments']['tid']['default_argument_fixed'] = implode('+', $tids);
        $store_view->display[$cur_display_id]->display_options['arguments']['tid']['validate_argument_vocabulary'][$tags_vid] = $tags_vid;
      }
      
      if (isset($values['topspin_store_display_categories_group_order'])) {
        // Update category sort settings.
        $category_sort = $values['topspin_store_display_categories_group_order'];
        $sorts = $store_view->display[$cur_display_id]->display_options['sorts'];
        if ($sorts) {
          $new_sorts = array();
          // Alter sorts.
          switch ($category_sort) {
            case 'newest_to_oldest':
              $new_sorts['created'] = $sorts['created'];
              $new_sorts['created']['order'] = 'DESC';
              $new_sorts['field_topspin_weight_value'] = $sorts['field_topspin_weight_value'];
              $new_sorts['name'] = $sorts['name'];
              break;
            case 'oldest_to_newest':
              $new_sorts['created'] = $sorts['created'];
              $new_sorts['created']['order'] = 'ASC';
              $new_sorts['field_topspin_weight_value'] = $sorts['field_topspin_weight_value'];
              $new_sorts['name'] = $sorts['name'];
              break;
            case 'spin_tag':
            case 'product_type':
              $new_sorts['name'] = $sorts['name'];
              $new_sorts['created'] = $sorts['created'];
              $new_sorts['created']['order'] = 'DESC';
              $new_sorts['field_topspin_weight_value'] = $sorts['field_topspin_weight_value'];
              break;
            case 'custom':
              $new_sorts['field_topspin_weight_value'] = $sorts['field_topspin_weight_value'];
              $new_sorts['field_topspin_weight_value']['order'] = 'ASC';
              $new_sorts['created'] = $sorts['created'];
              $new_sorts['created']['order'] = 'DESC';
              $new_sorts['name'] = $sorts['name'];
              break;
          }
          $store_view->display[$cur_display_id]->display_options['sorts'] = $new_sorts;
        }
      }
      
      if (isset($values['topspin_store_details_page_per_offer'])) {
        $store_view->display[$cur_display_id]->display_options['fields']['title']['alter']['make_link'] = $values['topspin_store_details_page_per_offer'];
        $store_view->display[$cur_display_id]->display_options['fields']['field_topspin_image_fid']['format'] = $values['topspin_store_details_page_per_offer'] ? 'product_medium_linked' : 'product_medium|fancybox';
      }
    }
    
    // Most Popular block.
    if (isset($values['topspin_store_popular_block_offers_count'])) {
      $store_view->display['block_1']->display_options['items_per_page'] = $values['topspin_store_popular_block_offers_count'];
    }
    
    $enabled_offers = array();
    
    // Save enabled offer blocks.
    // Get all offers.
    $result = topspin_request('offers');
    if ($result->offers) {
      // Build a structured array of offers to enable as blocks.
      foreach ($values as $name => $value) {
        // Add enabled blocks to $enabled_offers array for saving later.
        if (strpos($name, 'topspin_enabled_offer_block_') === 0) {
          $offer_id = array_pop(explode('_', $name));
          $enabled_offers[$offer_id] = topspin_get_offer_by_id($offer_id, $result->offers);
        }
      }
    }
    // Save enabled blocks.
    topspin_enabled_offer_blocks($enabled_offers);
    // Save and clear caches.
    $store_view->save();
    menu_rebuild();
    cache_clear_all('*', 'cache_views', TRUE);
    cache_clear_all();
    views_object_cache_clear('view', $store_view->name);
    
    // testing
    $store_view = views_get_view('topspin_store', TRUE);
    
    if ($tax_menu_rebuild_needed) {
      topspin_menus_rebuild();
    }
  }
  
  // We're getting view configs.
  else {
    
    return $existing_values;
    
  }
}

function topspin_store_menu_context_handler($context, $action) {
  if ($context) {
    $status = variable_get('context_status', array());
    if ($action == 'disable') {
      $status[$context->name] = true;
    }
    if ($action == 'enable' && $status[$context->name]) {
      unset($status[$context->name]);
    }
    variable_set('context_status', $status);
    context_invalidate_cache();
  }
}
