<?php
// $Id: $

/**
 *
 * @file
 * Management functionality for Topspin Media CCK module.
 *
 */

/**
 * Content form.
 * 
 * @return struct
 * - Settings form array.
 */
function topspin_cck_content_form() {
  // Account.
  $form['imports'] = array('#type' => 'fieldset', '#title' => t('Imports'), '#description' => '', '#collapsible' => TRUE);
  $form['imports']['topspin_override_existing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overwrite existing content?'),
    '#description' => '<span style="color:#ff0000;">'. t('Use the overwrite option with caution. Local overrides to existing imported nodes will be wiped out.') .'</span>',
  );
  
  $form['imports']['topspin_import_button'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh Topspin Data'),
  );
  
  $form['imports']['topspin_rebuild_description'] = array(
    '#value' => '<div class="form-item"><br /><label>'. t("Force store category menu rebuild. (This is only needed if menu items get out of sync)") .'</label></div>',
  );
  
  $form['imports']['topspin_rebuild_menus_button'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild Store category menus'),
  );
  
  // Add a custom submit handler and make sure it gets hit last.
  $form['#submit'][] = 'topspin_cck_settings_form_submit';
  
  return $form;
}

/**
 * Settings form submit callback. Handle saving of enabled blocks.
 *
 * @param struct
 * - Form.
 * @param struct
 * - Form state.
 */
function topspin_cck_settings_form_submit($form, &$form_state) {
  switch ($form_state['clicked_button']['#value']) {
    case t('Refresh Topspin Data'):
      
      $override = NULL;
      if ($form_state['values']['topspin_override_existing']) {
        $override = 1;
      }
      topspin_cck_import_offers($override);
      
      topspin_menus_rebuild();
      
      break;
    case t('Rebuild Store category menus'):
      
      variable_set('topspin_store_menu_rebuild_needed', 0);
      topspin_menus_rebuild();
      
      break;
  }
}

/**
 * Admin additions to hook_form_alter().
 */
function _topspin_cck_admin_form_alter(&$form, &$form_state, $form_id) {
  // Add Import related stuff to Topspin configs.
  if ($form_id == 'topspin_settings_form') {
    $form['imports'] = array('#type' => 'fieldset', '#weight' => 1, '#title' => t('Product Importing'), '#description' => '', '#collapsible' => TRUE, '#collapsed' => FALSE);
    $form['imports']['topspin_cck_import_on_cron'] = array(
      '#type' => 'checkbox',
      '#title' => t('Import Topspin products on CRON?'),
      '#description' => t("Should new products be imported and existing products be updated when CRON is run?"),
      '#options' => array(
        0 => t('No'),
        1 => t('Yes'),
      ),
      '#default_value' => variable_get('topspin_cck_import_on_cron', 1),
    );
    $form['imports']['topspin_cck_offer_import_publish'] = array(
      '#type' => 'checkbox',
      '#title' => t('Import and publish automatically?'),
      '#description' => t("How would you like offers imported? Publish all automatically? Or import and wait for me to review and publish? <em>Note: This will never change the publish status of existing product nodes.</em>"),
      '#options' => array(
        0 => t('No'),
        1 => t('Yes'),
      ),
      '#default_value' => variable_get('topspin_cck_offer_import_publish', 0),
    );
  }
}
